package Cli;

import Cli.Screen.AddProductScreen;
import Cli.Screen.DisplayProductScreen;
import Cli.Screen.FinalizeBasketScreen;
import Cli.Screen.RemoveProductScreen;
import Domain.Basket;
import Domain.ListProduct;
import Domain.Product;

import java.util.Scanner;

public class Application {
    static void application() {
        Basket basket = new Basket();
        ListProduct listproduct = new ListProduct();

        Product product1 = new Product("pomme", 10);
        Product product2 = new Product("poire", 50);
        Product product3 = new Product("fraise", 100);


        listproduct.add(product1);
        listproduct.add(product2);
        listproduct.add(product3);


        Scanner scanner = new Scanner(System.in);

        DisplayProductScreen displayproduct = new DisplayProductScreen(listproduct);
        displayproduct.display(scanner);


        AddProductScreen addproduct = new AddProductScreen(basket, listproduct);
        addproduct.display(scanner);


        System.out.println("-------------------");
        System.out.println("Votre panier : ");
        for (Object elem : basket.getBasket()) {
            System.out.println(elem);

        }
        System.out.println("Le total de votre panier est de : "+ basket.getPriceTotal()+ " euros.");
        String choice;


        do {
            System.out.println("A : Ajouter un produit. X : Supprimer un produit. V : Valider la commande.");
            choice = scanner.nextLine();

            if (choice.equalsIgnoreCase("A")) {
                AddProductScreen addproduct1 = new AddProductScreen(basket, listproduct);
                addproduct1.display(scanner);
                for (Object elem : basket.getBasket()) {
                    System.out.println(elem);

                }
            } else if (choice.equalsIgnoreCase("X")) {
                RemoveProductScreen removeproductscreen= new RemoveProductScreen(basket, listproduct);
                removeproductscreen.display(scanner);
                for (Object elem : basket.getBasket()) {
                    System.out.println(elem);

                }

            }

        }
        while (!choice.equalsIgnoreCase("V"));

        FinalizeBasketScreen finalizebasket = new FinalizeBasketScreen(basket);
        finalizebasket.display(scanner);
    }
}
