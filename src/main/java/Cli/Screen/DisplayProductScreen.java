package Cli.Screen;

import Domain.ListProduct;


import java.util.Scanner;

public class DisplayProductScreen implements Screen {
    private ListProduct listproduct;


    public DisplayProductScreen(ListProduct listproduct) {
        this.listproduct = listproduct;
    }


    @Override
    public void display(Scanner scanner) {
        System.out.println("""
                -------------------
                Bienvenue chez Zenika 
                Voici le catalogue :
                -------------------   """);

        for (Object elem : listproduct.getListproduct()) {
            System.out.println(elem + " euros");

        }
    }
}



