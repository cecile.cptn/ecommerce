package Cli.Screen;

import java.util.Scanner;

public class InputManager {
    private static Scanner scanner = new Scanner(System.in);

    public static String getProductChoiceCustomer(){
        return scanner.nextLine();

    }

    public static int getAmountChoiceCustomer (){
        return Integer.parseInt(scanner.nextLine());
    }
}
