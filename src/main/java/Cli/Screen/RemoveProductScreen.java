package Cli.Screen;

import Domain.Basket;
import Domain.ListProduct;
import Domain.Product;

import java.util.List;
import java.util.Scanner;

public class RemoveProductScreen implements Screen {
    private Basket basket;
    private ListProduct listproduct;


    public RemoveProductScreen(Basket basket, ListProduct listproduct) {
        this.basket = basket;
        this.listproduct = listproduct;

    }

    @Override
    public void display(Scanner scanner) {

        String productChoiceCustomer;
        Product productToRemove;

        do {
            System.out.println("Quel produit supprimer ? ");
            productChoiceCustomer = InputManager.getProductChoiceCustomer();
            productToRemove = getProductByName(basket.getBasket(), productChoiceCustomer);
        } while (!basket.getBasket().contains(productToRemove));


        try{
            System.out.println("Entrer la quantité souhaitée : ");
            int amountCustomer = InputManager.getAmountChoiceCustomer();
            for (int i = 0; i < amountCustomer; i++) {
                basket.remove(productToRemove);

            }
        }catch (NumberFormatException e){
            System.out.println("Nombre non valide");
        }
    }

    public Product getProductByName(List<Product> products, String productName) {
        for (Product p : products){
            if (productName.equals(p.getName())){
                return p;
            }
        }
        return null;
    }
}



