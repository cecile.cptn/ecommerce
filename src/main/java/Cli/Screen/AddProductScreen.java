package Cli.Screen;

import Domain.Basket;
import Domain.ListProduct;
import Domain.Product;

import java.util.List;
import java.util.Scanner;

public class AddProductScreen implements Screen {
    private Basket basket;
    private ListProduct listproduct;


    public AddProductScreen(Basket basket, ListProduct listproduct) {
        this.basket = basket;
        this.listproduct = listproduct;
    }

    @Override
    public void display(Scanner scanner) {
        String productChoiceCustomer;
        Product productToAdd;

        do {
            System.out.println("Entrer le produit : ");
            productChoiceCustomer = InputManager.getProductChoiceCustomer();
            productToAdd = getProductByName(listproduct.getListproduct(), productChoiceCustomer);
        } while (!listproduct.getListproduct().contains(productToAdd));


        changeBasketAmount(productToAdd);


    }

    private void changeBasketAmount(Product productToAdd) {
        System.out.println("Entrer la quantité souhaitée : ");
        try {
            int amountCustomer = InputManager.getAmountChoiceCustomer();
            for (int i = 0; i < amountCustomer; i++) {
                basket.add(productToAdd);

            }
        } catch (NumberFormatException e) {
            System.out.println("Nombre non valide");
        }
    }

    public Product getProductByName(List<Product> products, String productName) {
        for (Product p : products) {
            if (productName.equals(p.getName())) {
                return p;
            }
        }
        return null;
    }


}

