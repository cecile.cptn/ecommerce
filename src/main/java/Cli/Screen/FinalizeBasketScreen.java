package Cli.Screen;

import Domain.Basket;
import java.time.LocalDate;
import java.util.Scanner;


public class FinalizeBasketScreen implements Screen {
    private Basket basket;
    public FinalizeBasketScreen(Basket basket) {
        this.basket = basket;
    }


    @Override
    public void display(Scanner scanner) {
        System.out.println("-------------------");
        System.out.println("Entrer votre adresse : ");
        String adress = scanner.nextLine();
        for (Object elem : basket.getBasket()) {
            System.out.println(elem);

        }
        System.out.println("Votre commande a été validée le : "+ LocalDate.now() + ". Elle sera livrée à : " + adress + ", le "+ LocalDate.now().plusDays(3)+". Le prix total de votre panier est de : " + basket.getPriceTotal() + " euros.");
    }
    }

