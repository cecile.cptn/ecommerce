package Cli.Screen;

import java.util.Scanner;

public interface Screen {
    void display(Scanner scanner);
}
