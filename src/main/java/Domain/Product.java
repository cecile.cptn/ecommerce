package Domain;

public class Product {
    private String name;
    private int price;


    public Product(String name, int prix) {
        if (name.equals(" ") || name.equals("")){
            throw new IllegalArgumentException();
        }
        this.name = name;
        if (prix <0){
            throw new IllegalArgumentException();
        }
        this.price = prix;
    }


    @Override
    public String toString() {
        return name + " " + price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

}

