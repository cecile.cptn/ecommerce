package Domain;

import Domain.Product;

import java.util.ArrayList;

public class Basket {

    private ArrayList <Product>basket = new ArrayList<>();
    private int priceTotal =0;

    public void setBasket(ArrayList<Product> basket) {
        this.basket = basket;
    }

    public void setPriceTotal(int priceTotal) {
        this.priceTotal = priceTotal;
    }

    public ArrayList<Product> getBasket() {
        return basket;
    }

    public int getPriceTotal() {
        return priceTotal;
    }

    public void add (Product product) {
        basket.add(product);
        priceTotal+= product.getPrice();

    }

    public void remove(Product product) {
        basket.remove(product);
        priceTotal-= product.getPrice();
        System.out.println(product.getName() + " enlevé du panier");
    }

}




