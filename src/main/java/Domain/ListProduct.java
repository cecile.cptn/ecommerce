package Domain;

import java.util.ArrayList;

public class ListProduct {
    private ArrayList<Product> listproduct = new ArrayList<>();

    public ListProduct(ArrayList<Product> listproduct) {
        this.listproduct = listproduct;
    }

    public ListProduct() {

    }


    public ArrayList<Product> getListproduct() {
        return listproduct;
    }

    public void setListproduct(ArrayList<Product> listproduct) {
        this.listproduct = listproduct;
    }
    public void add (Product product) {
        listproduct.add(product);


    }
}
