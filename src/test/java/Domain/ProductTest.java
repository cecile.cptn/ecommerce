package Domain;

import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {
    @Test
    void priceMustBePositive() {
        try {
            Product product = new Product("Banane", -10);
            Assertions.fail();
        } catch (IllegalArgumentException e) {
            System.out.println("Prix négatif");
        }
        }


    @Test
    void nameMustntBeEmpty() {
        try {
            Product product = new Product("", 10);
            Assertions.fail();
        } catch (IllegalArgumentException e) {
            System.out.println("Nom pas valide");
        }
    }

    }
